const Pemain = require("./pemain");


function ronaldo() {
    let data_pemain = new Pemain('Cristiano Ronaldo dos Santos Aveiro', 'Ronaldo', '5 Februari 1985', 'São Pedro, Funchal, ibu kota pulau Madeira', 'Portugal');

    console.log("------------------------------Data Pemain------------------------------");
    console.log(`Nama lengkap: ${data_pemain.fullname}`);
    console.log(`Nama data_panggilan: ${data_pemain.nickname}`);
    console.log(`Tanggal lahir: ${data_pemain.date}`);
    data_pemain.umur(1985);
    console.log(`Tempat kelahiran: ${data_pemain.birth_place}`);
    console.log(`Asal negara: ${data_pemain.country}`);

    console.log("\n--------------------------------Career--------------------------------");
    data_pemain.career("Manchester United", "Machester United, Real Madrid, Juventus", "Portugal U15, Portugal U17, Portugal U20, Portugal U21, Portugal U23, Portugal");

    console.log("\n--------------------------------Style Play--------------------------------");
    data_pemain.styleplay("Secara luas dianggap sebagai salah satu dari dua pemain terbaik di dunia dan sebagai salah satu pemain terbaik yang pernah bermain, Cristiano Ronaldo memainkan peran menyerang, paling sering bermain baik sebagai striker atau sebagai pemain sayap, dan dikenal karena finishing, kecepatan, dribbling, positioning, passing dan kemampuan crossing. Dia mampu bermain di kedua sayap juga ditengah, membuatnya menjadi penyerang yang sangat serbaguna.");
}

function messi() {
    let data_pemain = new Pemain('Lionel Andrés Messi', 'Messi', '24 Juni 1987', ' Rosario, Provinsi Santa Fe', 'Argentina');

    console.log("------------------------------Data Pemain------------------------------");
    console.log(`Nama lengkap: ${data_pemain.fullname}`);
    console.log(`Nama data_panggilan: ${data_pemain.nickname}`);
    console.log(`Tanggal lahir: ${data_pemain.date}`);
    data_pemain.umur(1987);
    console.log(`Tempat kelahiran: ${data_pemain.birth_place}`);
    console.log(`Asal negara: ${data_pemain.country}`);

    console.log("\n--------------------------------Career--------------------------------");
    data_pemain.career("PSG", "Barcelona dan PSG", "Argentina U20, Argentina U23, Argentina");

    console.log("\n--------------------------------Style Play--------------------------------");
    data_pemain.styleplay("Messi telah dibandingkan dengan rekan senegaranya Diego Maradona, karena gaya bermain dan perawakan mereka yang serupa, memberinya pusat gravitasi yang lebih rendah daripada kebanyakan pemain, sehingga dia lebih lincah dengan arah perubahan yang lebih cepat, membantu dia untuk menghindari tackle. Posturnya yang pendek dan kaki yang kuat memungkinkan dia untuk unggul dalam ledakan singkat akselerasi dan langkahnya yang cepat memungkinkan dia untuk tetap mengontrol bola saat menggiring bola dengan kecepatan.");
}

const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
  });
readline.question('Pilih Ronaldo(R) atau Messi(M)?\n', name => {
    if (`${name}` == "R") {
        console.log("\nPilihanmu adalah Ronaldo suiii!!!\n");
        ronaldo();
    }
    else if (`${name}` == "M") {
        console.log("\nPilihanmu adalah Messi\n");
        messi();
    }
    readline.close();
  });
