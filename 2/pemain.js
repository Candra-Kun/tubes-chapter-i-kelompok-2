class pemain {
    constructor(fullname, nickname, date, birth_place, country) {
        this.fullname = fullname;
        this.nickname = nickname;
        this.date = date;
        this.birth_place = birth_place;
        this.country = country;
    }

    umur(birth_year) {
        const date = new Date();
        console.log('Umur: ', date.getFullYear() - birth_year);
    }

    career(club, history_club, international) {
        console.log('Club now: ', club);
        console.log('Club: ', history_club);
        console.log('International Career: ', international);
    }
    styleplay(style) {
        console.log(style);
    }
}

module.exports = pemain;